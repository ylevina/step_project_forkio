# Step Project Forkio

Layout given: https://www.figma.com/file/8TlSdTK6GHiBx8r01bPAVx/Forkio?node-id=0%3A27
There are 3 resolutions of content width: 320px, 768px and 1200px.
Layout must be done with all the principles of adaptive and responsive design.

## Technologies used: 
- HTML
- CSS
- SASS
- node.js
- npm
- gulp
- js
- git

## Participants: 
- Kateryna Sosnikova 
- Yuliia Levina

### Sections done by Kateryna Sosnikova (student 1 tasks): 
- header
- intro
- testimonials

### Sections done by Yuliia Levina (student 2 tasks): 
- editor
- features
- subscription